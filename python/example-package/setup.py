#!/usr/bin/env python

from setuptools import setup, find_packages
import os
import sys

if os.path.exists('README.md'):
    import pypandoc

    long_description = pypandoc.convert('README.md', 'rst')
else:
    long_description = ''

version = sys.version_info[:2]
if version < (3, 0):
    print('Python version 3.0 or later is required ' +
          '({}.{} detected).'.format(*version))
    sys.exit(1)

setup(
    name='example-package',
    version='0.1.0',
    license='GPL-3.0',
    author='Severen Redwood',
    author_email='severen@shrike.me',
    url='https://github.com/SShrike/playground',
    description='An awesome example package!.',
    long_description=long_description,
    packages=find_packages(exclude=['tests']),

    entry_points={'console_scripts': [
        'example-package = example-package.main:main'
    ]}
)
