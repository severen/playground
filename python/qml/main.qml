import QtQuick 2.5
import QtQuick.Controls 1.4

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Test Application")

    Button {
        text: qsTr("Hello world!")
        onClicked: console.log("Hello world!")
        anchors.centerIn: parent
    }
}
