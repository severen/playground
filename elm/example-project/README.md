# Example Elm Project

An example [Elm](http://elm-lang.org/) project that makes use of the following:

* [Webpack](https://webpack.github.io/), with:
  * Minification support.
  * ES6 support.
  * [Sass (SCSS)](http://sass-lang.com/) support.
  * [Autoprefixer](https://github.com/postcss/autoprefixer) support.
  * [Development server with live reloading](https://webpack.github.io/docs/webpack-dev-server.html).
  * [Hot module replacement (HMR)](https://webpack.github.io/docs/hot-module-replacement.html).
* [Zurb Foundation 6 for Sites](http://foundation.zurb.com/sites.html)
* [elm-html](http://package.elm-lang.org/packages/elm-lang/html/latest/),
  specifically [`Html.App`](http://package.elm-lang.org/packages/elm-lang/html/latest/Html-App).

## Usage

First, install Elm:

* Prebuilt (easiest): `npm install -g elm`.
* From distro package:
  * Arch Linux: `pacaur -S elm-platform`.

Then, install Elm's dependencies with `elm package install`.

And finally, run `npm start` to run the local development server; to build and
bundle for production, use `npm run build`.
