import QtQuick 2.5
import QtQuick.Controls 1.4
import Hello 1.0

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Test Application")

    Hello {
        id: hello
    }

    Button {
        text: qsTr("Hello world!")
        onClicked: hello.hello()
        anchors.centerIn: parent
    }
}
