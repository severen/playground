use std::io;
use std::io::Write;
use std::thread::sleep;
use std::time::Duration;

fn main() {
    spinner();
}

fn spinner() {
    let mut stdout = io::stdout();
    let spinner = ["⠋", "⠙", "⠹", "⠸", "⠼", "⠴", "⠦", "⠧", "⠇", "⠏"];

    println!("Loading:");
    loop {
        for part in spinner.iter() {
            print!("\u{0008}{}", part);
            sleep(Duration::from_millis(50));
            stdout.flush().ok().unwrap();
        }
    }
}
