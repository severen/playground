#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QQmlEngine>

#include <string>
#include <iostream>
#include <csignal>

#include "hello.hpp"

using namespace std;

void signalHandler(int signum) {
    QApplication::quit();
}

int main(int argc, char *argv[]) {
    signal(SIGINT, signalHandler);

    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    qmlRegisterType<Hello>("Hello", 1, 0, "Hello");
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
