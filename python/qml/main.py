#!/usr/bin/env python3

import sys

from PyQt5.QtCore import QUrl
from PyQt5.QtGui import QGuiApplication
from PyQt5.QtQml import QQmlApplicationEngine

if __name__ == '__main__':
    app = QGuiApplication(sys.argv)

    engine = QQmlApplicationEngine(QUrl('main.qml'))
    engine.quit.connect(app.quit)

    sys.exit(app.exec_())
