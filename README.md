Playground
==========

A collection of various things in different languages for use as personal
references. There is not guarantee any of this code will be up to day with
current practices, etc.

All the code in this repository is under the Unlicense, so feel free to copy and
paste.
