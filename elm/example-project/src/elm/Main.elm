import Html exposing (..)
import Html.Attributes exposing (..)
import Html.App as Html
import Html.Events exposing ( onClick )

import Component.Hello exposing ( hello )

main : Program Never
main =
  Html.beginnerProgram { model = model, view = view, update = update }

-- MODEL
type alias Model = Int

model : number
model = 0

-- UPDATE
type Msg = NoOp | Increment

update : Msg -> Model -> Model
update msg model =
  case msg of
    NoOp -> model
    Increment -> model + 1

-- VIEW
view : Model -> Html Msg
view model =
  div [class "row"] [
    h1 [class ""] [ text "Example Website" ],

    hello model,
    p [] [ text "This is an example website." ],
    button [ class "expanded alert button", onClick Increment ] [
      text "Press Me"
    ]
  ]
