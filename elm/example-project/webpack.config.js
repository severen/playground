const path = require('path');
const webpack  = require('webpack');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

// Determine the build target environment.
let TARGET_ENV = process.env.npm_lifecycle_event === 'build' ? 'production' : 'development';

// Common, shared config.
let commonConfig = {
    output: {
        path: path.resolve(__dirname, 'dist/'),
        filename: '[hash].js',
    },

    resolve: {
        modulesDirectories: ['node_modules'],
        extensions: ['', '.js', '.elm'],
        alias: { foundation: 'foundation-sites/js/foundation.core' },
    },

    module: {
        noParse: /\.elm$/,
        loaders: [{
            test: /\.(eot|ttf|woff|woff2|svg)$/,
            loader: 'file-loader',
        },
        {
            test: /\.js$/,
            exclude: /(node_modules)/,
            loader: 'babel',
            query: {
                presets: ['es2015'],
            },
        },
        {
          test: /(foundation\.core)/,
          loader: 'exports?foundation=jQuery.fn.foundation',
        }],

        sassLoader: {
            precision: 10, // Set precision for Foundation Sites.
        },
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: 'src/index.html',
            inject: 'body',
            filename: 'index.html',
        }),

        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
        }),
    ],

    postcss: [
        autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9', 'and_chr >= 2.3'],
        }),
    ],
}

// Development environment specific settings.
if (TARGET_ENV === 'development') {
    console.log('Serving locally...');

    module.exports = merge(commonConfig, {
        entry: [
            'webpack-dev-server/client?http://localhost:8080',
            path.join(__dirname, 'src/index.js'),
        ],

        devServer: {
            inline:   true,
            progress: true,
        },
        devtool: 'source-map',
        module: {
            loaders: [{
                test:    /\.elm$/,
                exclude: [/elm-stuff/, /node_modules/],
                loader:  'elm-hot!elm-webpack?verbose=true&warn=true',
            },
            {
                test: /\.(css|scss)$/,
                loaders: [
                    'style-loader',
                    'css-loader?sourceMap',
                    'postcss-loader',
                    'sass-loader?sourceMap',
                ],
            }],
        },
    });
}

// Production environment specific settings.
if (TARGET_ENV === 'production') {
    console.log('Building for production...');

    module.exports = merge(commonConfig, {
        entry: path.join(__dirname, 'src/index.js'),

        module: {
            loaders: [{
                test:    /\.elm$/,
                exclude: [/elm-stuff/, /node_modules/],
                loader:  'elm-webpack',
            },
            {
                test: /\.(css|scss)$/,
                loader: ExtractTextPlugin.extract( 'style-loader', [
                    'css-loader',
                    'postcss-loader',
                    'sass-loader',
                ]),
            }],
        },

        plugins: [
            new CopyWebpackPlugin([{
                from: 'src/img/',
                to: 'static/img/',
            },
            {
                from: 'src/favicon.ico',
            }]),

            new webpack.optimize.OccurenceOrderPlugin(),
            new webpack.optimize.DedupePlugin(),

            // Extract CSS into a separate file.
            new ExtractTextPlugin( './[hash].css', { allChunks: true } ),

            // Minify JS/CSS.
            new webpack.optimize.UglifyJsPlugin({
                minimize: true,
                compressor: { warnings: false },
                mangle: true,
            }),
        ],
    });
}
