// Can also use #pragma once instead of an include guard.
#ifndef HELLO_HPP
#define HELLO_HPP

#include <QObject>
#include <QString>

class Hello: public QObject {
    Q_OBJECT
public:
    Q_INVOKABLE void hello();
};

#endif /* HELLO_HPP */
