import './styles/main.scss';

// Foundation Sites Initialisation
import 'what-input';
import 'foundation-sites';
// FIXME: Dirty hack, see: https://github.com/zurb/foundation-sites/issues/7386.
$.fn.foundation = function(method) {
    const type = typeof method,
        meta = $('meta.foundation-mq'),
        noJS = $('.no-js');

    if (!meta.length) {
        $('<meta class="foundation-mq">').appendTo(document.head);
    }
    if (noJS.length) {
        noJS.removeClass('no-js');
    }

    if (type === 'undefined') {
        Foundation.MediaQuery._init();
        Foundation.reflow(this);
    } else if (type === 'string') {
        let args = Array.prototype.slice.call(arguments, 1);
        let plugClass = this.data('zfPlugin');

        if (plugClass !== undefined && plugClass[method] !== undefined) {
            if (this.length === 1) {
                plugClass[method].apply(plugClass, args);
            } else {
                this.each(function(i, el){
                    plugClass[method].apply($(el).data('zfPlugin'), args);
                });
            }
        } else {
            throw new ReferenceError(
                "We're sorry, '" + method + "' is not an available method for "
                + (plugClass ? functionName(plugClass) : 'this element') + '.'
            );
        }
    } else {
        throw new TypeError(
            `We're sorry, ${type} is not a valid parameter. You must use a \
            string representing the method you wish to invoke.`
        );
    }
    return this;
};
$(document).foundation();

// Inject the Elm application into div#elm-main.
import Elm from './elm/Main';
Elm.Main.embed(document.getElementById('elm-main'));
